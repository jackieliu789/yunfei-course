# 云飞在线互动课堂

```html
1：支持手机端、PC端一键开播
2：支持老师、机构入驻
3：支持上传音频、视频知识付费
4：支持一对一、小班课(50人以内)、大班课(不限制人数)互动直播
5：支持点赞、连麦、禁言、聊天等互动操作
6：支持输入正确邀请码进入互动直播课堂
7：更多功能等待你的解锁
```

#### 联系作者

1.  如需体验或者购买最新版系统请添加作者微信
2.  作者微信（添加时请备注“互动课堂”）：<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/kefu.png" alt="云飞进销存" width="25%"/>

#### 软件截图

![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/1.png)
![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/2.png)
![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/3.png)
![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/3.jpg)
![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/5.jpg)
![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/6.jpg)
![云飞在线互动教育](https://gitee.com/jackieliu789/yunfei-course/raw/master/images/7.jpg)